package com.moueslati.model;

public class Company {
	
	
	public Company(String siretNbr, String naming) {
		this.siretNbr = siretNbr;
		this.naming = naming;
	}


	protected String siretNbr;
	protected String naming;
	protected Double taxPercentage;
	
	
	public String getSiretNbr() {
		return siretNbr;
	}

	public String getNaming() {
		return naming;
	}

	public Double getTaxPercentage() {
		return taxPercentage;
	}


	public Double calculateTaxes(Integer ca) {
		
		return taxPercentage * ca;
		
	}

}

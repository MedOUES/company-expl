package com.moueslati.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.moueslati.model.AutoCompany;
import com.moueslati.model.Company;
import com.moueslati.model.SASCompany;

public class ModelTestClass {
	
	
	
	Company autoCompany;
	Company sasCompany;
	
	
	@Before
	public void testStart() {
		
		
		autoCompany = new AutoCompany("", "");
		sasCompany = new SASCompany("", "");
		
		
	}
	
	
	
	@Test
	public void AutoCompany_Test() {
		
		Assert.assertEquals(0.00,autoCompany.calculateTaxes(null).doubleValue(),0);
		
	}
	
	
	
	@Test
	public void SASCompany_Test() {
		
		Assert.assertEquals(0.00,sasCompany.calculateTaxes(null).doubleValue(),0);
		
		
	}

}
